console.log("\n No. 1")

function range(startNum, finishNum) {
    if (!startNum||!finishNum){
        return (-1);
    }
    let result = [];
    let step = startNum <= finishNum ? 1 : -1;
    for ( let i = startNum; step >= 0  ? i <= finishNum : i >= finishNum; i+=step) {
        result.push(i);
    }
    return result ;
}
console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range())

console.log("\n No. 2")

const rangewithstep = (start, stop, step) => 
Array.from({ length: (stop - start) / step + 1}, (_, i) => start + (i * step));

console.log(rangewithstep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangewithstep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangewithstep(5, 2, -1)) // [5, 4, 3, 2]
console.log(rangewithstep(29, 2, -4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("\n No. 4")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling() {
    for (var i = 0; i<input.length; i++) {
        console.log("Nomor ID : " + input[i][0])
        console.log("Nama Lengkap : " + input[i][1])
        console.log("TTL : " + input[i][2])
        console.log("Hobi : " + input[i][3] + "\n")
    }
}
dataHandling()

console.log("\n No. 5")

var string;
function balikKata(string) {
    stringArr = '';
    c = string.length - 1;
    
    while(c >= 0) {
    stringArr += string[c];
    c--;
    }
    return stringArr;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 

/*console.log("\n No. 6")
var input = ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"];
var nama;
var provinsi;
var ttl;
var jk;
var pend;

length = input.length
id = input.splice(1, 1);
nama = input.splice(1, 1);
provinsi = input.splice(1, 1);
ttl = input.splice(1, 1);
jk = input.splice(1, 1);
pend = input.splice(1, 1);

var tglsplit = ttl
var bulan = 

if(hari >=1 && hari <=31) {
    if(tahun >=1900 && tahun <=2200) {

        switch(bulan){
            case 1: {console.log(hari + ' Januari ' + tahun); break;}
            case 2: {console.log(hari + ' Februari ' + tahun); break;}
            case 3: {console.log(hari + ' Maret ' + tahun); break;}
            case 4: {console.log(hari + ' April ' + tahun); break;}
            case 5: {console.log(hari + ' Mei ' + tahun); break;}
            case 6: {console.log(hari + ' Juni ' + tahun); break;}
            case 7: {console.log(hari + ' Juli ' + tahun); break;}
            case 8: {console.log(hari + ' Agustus ' + tahun); break;}
            case 9: {console.log(hari + ' September ' + tahun); break;}
            case 10: {console.log(hari + ' Oktober ' + tahun); break;}
            case 11: {console.log(hari + ' November ' + tahun); break;}
            case 12: {console.log(hari + ' Desember ' + tahun); break;}
            default: {console.log("Tidak Ada Yang Dipilih"); break;}
        }
    }
    
}

console.log(id)
console.log(nama)
console.log(provinsi)
console.log(ttl)
console.log(jk)
console.log(pend)*/
