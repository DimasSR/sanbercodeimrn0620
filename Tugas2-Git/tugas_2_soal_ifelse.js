var nama = "Perta"
var peran = "Werewolf"

if ( nama == "" ) {
    console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
    console.log("Nama harus diisi!")
} else if ( peran == "") {
    console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
    console.log("Hallo " + nama + ", pilih peranmu untuk memulai game!")
} else if(peran == "Penyihir"){
    console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Hallo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi Werewolf")
} else if(peran == "Guard"){
    console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Hallo Guard " + nama + ", kamu dapat melindungi temanmu dari serangan Werewolf")
} else if(peran == "Werewolf"){
    console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Hallo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam!")
} else {
    console.log("selesai")
}    